# README #

### Интернет-магазин «Маркетплейс» ###
Должны быть реализованы 3 роли «ADMIN», «SELLER» и «CUSTOMER»
* 1.Для роли «ADMIN» должны быть реализованы функции:
  * 1.Просмотр своего профиля
  * 2.Корректировка своих адреса и телефона
  * 3.Просмотра списка пользователей с поиском по email, роли и пагинацией
  * 4.Корректировка у пользователей статуса «В архиве»
  * 5.Просмотр списка продуктов с поиском по продавцу, пагинацией
  * 6.Корректировка у продуктов признаков «В архиве» и «Не доступен»
  * 7.Просмотр списка покупателей с поиском по email, пагинацией
  * 8.Корректировка у покупателя бонусов и скидок
* 2.Для роли «SELLER» должны быть реализованы функции:
  * 1.Просмотра своего профиля
  * 2.Корректировка своих реквизитов адреса и телефона
  * 3.Просмотр своих продуктов с поиском по категории, категории продукта, наименованию и пагинацией
  * 4.Просмотр картинок продукта
  * 5.Ввод, корректировка продуктов, в том числе признака «В архиве» и картинки продукта
* 3.Для роли «CUSTOMER» должны быть реализованы функции:
  * 1.Просмотр своего профиля
  * 2.Корректировка своих адреса и телефона
  * 3.Просмотр списка продуктов с поиском по категории, категории продукта, наименованию и пагинацией
  * 4.Сохранение выбранного товара в корзине
  * 5.Просмотр корзины с выводом примененной скидки или бонуса. Для корзины должна быть выведена рассчитанная стоимость доставки
  * 6.Оплата продуктов в корзине и создание заказа в статусе «Оплачен»
  * 7.Просмотр списка своих заказов с поиском по номеру заказа, статусу заказа и пагинацией
  * 8.Подтверждение доставки, перевод заказа в статус «Доставлен»

### Спецификации ###

* Spring boot.
*  REST API without from only requests from resources.
*  Security on TOKEN
*  DB postgresql or MongoDB
*  JPA DB objects: USER(status, role), SELER, CUSTOMER, PRODUCT(category), ORDER(status, delivery_status), Product_Order.
*  Test MockMvc and zonky)))???

* [Learn Markdown](https://bitbucket.org/Guhar4k/marketplace/src/master/)

### Стек технологий ###
* Jira, доска Kanban
* Java 11
* Spring Boot
* Git, Bitbucket
* Spring Security
* Maven
* Web:
* Spring Web MVC
* Tomcat
* JSP / Freemarker / Thymeleaf
* Работа с БД:
* Spring Data JPA
* PostgreSQL / MongoDB
* Hibernate
* ~SQL, JPQL, CriteriaAPI
* Тестирование:
* Junit 5
* Mockito
* SpringTest
* ~ Очереди сообщения - RabbitMQ / Kafka
* ~ Docker

### Pages with permit’s ###

* signIn(signIn form) permitAll
* signUp(signUp form) permitAll
* User(info and role) for User not metter what’s role
* Market(products) for Customer
* Orders(orders) for Customer
* Seller(products) for Seller
* DeliveryOptional(state) for Customer

### Controllers ###

![img.png](img.png)

### Модели данных ###
* User
* UserRole
* UserStatus
* UserSeller
* UserCustomer
* Order
* Order_status
* Product_Order
* Delivery
* Product
* Category
* Photo storage
* Seller

