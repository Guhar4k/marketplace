package marketplace.services;

import marketplace.models.entities.User;

public interface ConfirmEmailService {
    void sendEmailConfirmRegistration(User user);
    boolean checkRegistrationToken(String token);
}
