package marketplace.services.impl;

import lombok.RequiredArgsConstructor;
import marketplace.models.entities.RefreshToken;
import marketplace.repositories.RefreshTokenRepository;
import marketplace.repositories.UserRepository;
import marketplace.services.RefreshTokenService;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RefreshTokenServiceImpl implements RefreshTokenService {
    private final UserRepository userRepository;
    private final RefreshTokenRepository refreshTokenRepository;

    public static final Long REFRESH_TOKEN_DURATION_MS = 1000L * 60 * 60 * 24 * 7;

    @Override
    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    @Override
    public RefreshToken createRefreshToken(Long userId) {
        RefreshToken refreshToken = RefreshToken.builder()
                .user(userRepository.getById(userId))
                .expiryDate(Instant.now().plusMillis(REFRESH_TOKEN_DURATION_MS))
                .token(UUID.randomUUID().toString())
                .build();

        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    @Override
    public RefreshToken verifyExpiration(RefreshToken refreshToken) {
        if (refreshToken.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepository.delete(refreshToken);
            throw new IllegalArgumentException("Срок действия сессии закончен!");
        }
        return refreshToken;
    }
}
