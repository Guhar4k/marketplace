package marketplace.services.impl;

import lombok.RequiredArgsConstructor;
import marketplace.models.entities.User;
import marketplace.repositories.RedisRepository;
import marketplace.services.ConfirmEmailService;
import marketplace.services.UserService;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static marketplace.models.entities.User.State.CONFIRMED;

@Service
@RequiredArgsConstructor
public class ConfirmEmailServiceImpl implements ConfirmEmailService {
    private final RedisRepository redisRepository;
    private final JavaMailSender mailSender;
    private final UserService userService;

    @Override
    public void sendEmailConfirmRegistration(User user) {
        String token = UUID.randomUUID().toString();
        redisRepository.saveTokenAndUserId(token, user.getId());

        String recipientAddress = user.getEmail();
        String subject = "Registration Confirmation";
        String confirmationUrl = "/api/signUp/registrationConfirm/" + token;
        String message = "Для подтверждения почты перейдите по адресу: ";

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + "\r\n" + "http://localhost:8080" + confirmationUrl);

        mailSender.send(email);
    }

    @Override
    public boolean checkRegistrationToken(String token) {
        if (!redisRepository.exists(token)) {
            return false;
        }
        Long userId = redisRepository.getUserIdByToken(token);
        userService.updateUserStatus(userId, CONFIRMED);
        return true;
    }
}
