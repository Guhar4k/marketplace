package marketplace.services.impl;

import lombok.RequiredArgsConstructor;
import marketplace.models.entities.*;
import marketplace.models.dto.*;
import marketplace.repositories.*;
import marketplace.services.OrdersService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static marketplace.models.dto.OrderDto.from;
import static marketplace.models.dto.DeliveryDto.from;
import static marketplace.models.dto.OrderStatusDto.from;
import static marketplace.models.dto.ProductDto.from;

@Service
@RequiredArgsConstructor
public class OrdersServiceImpl implements OrdersService {
    private final OrdersRepository ordersRepository;
    private final OrderStatusesRepository orderStatusesRepository;
    private final DeliveriesRepository deliveriesRepository;
    private final ProductOrderRepository productOrderRepository;
    private final ProductsRepository productsRepository;
    private final CustomersRepository customersRepository;

    @Override
    public List<OrderDto> getOrders(int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by("id"));
        List<Order> result = ordersRepository.findAllByOrderStatus_Name("CREATED", pageRequest);
        return from(result);
    }

    @Override
    public OrderDto addOrder(OrderDto orderDto) {
        OrderStatus orderStatus = orderStatusesRepository.findByName("CREATED")
                .orElseThrow(() -> new IllegalArgumentException("Статус CREATED не добавлен в базу"));
        Optional<Customer> customer = customersRepository.findById(orderDto.getCustomerId());
        Order order = Order.builder()
                .orderDate(new Date())
                .orderNum(orderDto.getOrderNum())
                .orderSum(orderDto.getOrderSum())
                .orderStatus(orderStatus)
                .build();
        customer.ifPresent(order::setCustomer);
        return from(ordersRepository.save(order));
    }

    @Override
    public OrderDto updateOrder(Long orderId, OrderDto orderDto) {
        Order order = ordersRepository.getById(orderId);
        order.setOrderDate(new Date());
        order.setOrderNum(orderDto.getOrderNum());
        order.setOrderSum(orderDto.getOrderSum());
        ordersRepository.save(order);
        return from(order);
    }

    @Override
    public void deleteOrder(Long orderId) {
        Order order = ordersRepository.getById(orderId);
        Optional<OrderStatus> optionalStatus = orderStatusesRepository.findByName("DELETED");
        if (optionalStatus.isPresent()) {
            order.setOrderStatus(optionalStatus.get());
            ordersRepository.save(order);
        }
    }

    @Override
    public DeliveryDto addDeliveryToOrder(Long orderId, DeliveryDto deliveryDto) {
        Order order = ordersRepository.getById(orderId);
        Delivery delivery = deliveriesRepository.findByName(deliveryDto.getName())
                .orElseThrow(() -> new IllegalArgumentException("Доставка не найдена"));
        order.setDelivery(delivery);
        ordersRepository.save(order);
        return from(delivery);
    }

    @Override
    public List<OrderDto> getOrdersByUser(Long customerId, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by("id"));
        List<Order> result = ordersRepository.findAllByCustomerId(customerId, pageRequest);
        return from(result);
    }

    @Override
    public OrderDto getOrderById(Long orderId) {
        Order order = ordersRepository.getById(orderId);
        return from(order);
    }

    @Override
    public List<ProductDto> getProductsByOrderId(Long orderId) {
        List<ProductOrder> productOrders = productOrderRepository.findAllByOrderId(orderId);
        List<Product> products = productOrders.stream().map(ProductOrder::getProduct).collect(Collectors.toList());
        return from(products);
    }

    @Override
    public OrderStatusDto getOrderStatus(Long orderId) {
        Order order = ordersRepository.getById(orderId);
        return from(order.getOrderStatus());
    }

    @Override
    public DeliveryDto getDelivery(Long orderId) {
        Order order = ordersRepository.getById(orderId);
        return from(order.getDelivery());
    }

    @Override
    public List<ProductDto> addProductToOrder(Long orderId, Long productId) {
        Order order = ordersRepository.getById(orderId);
        Product product = productsRepository.getById(productId);
        ProductOrder productOrder = ProductOrder.builder()
                .order(order)
                .product(product)
                .price(product.getPrice())
                .quantity(1) //так как беру по id то подразумевается добавление только в единичном количестве
                .build();
        productOrderRepository.save(productOrder);
        return getProductsByOrderId(orderId);
    }

    @Override
    public List<ProductDto> changeProductQuantity(Long orderId, Long productId, Integer quantity) {
        ProductOrder expectedProductOrder = productOrderRepository.findAllByOrderId(orderId).stream()
                .filter(productOrder -> productOrder.getProduct().getId().equals(productId))
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Продукт или заказ не найден"));
        Product product = productsRepository.getById(productId);
        Integer productQuantity = product.getQuantity();
        //проверка на наличие и положить можно не больше чем есть на складе
        if (productQuantity != null && quantity > 0 && quantity <= productQuantity) {
            Double productPrice = product.getPrice() * quantity;
            expectedProductOrder.setQuantity(quantity);
            expectedProductOrder.setPrice(productPrice);
            productOrderRepository.save(expectedProductOrder);
        }
        return getProductsByOrderId(orderId);
    }

    @Override
    public void deleteProductInOrder(Long orderId, Long productId) {
        ProductOrder productOrder = productOrderRepository.findAllByOrderId(orderId).stream()
                .filter(expectedProductOrder -> expectedProductOrder.getProduct().getId().equals(productId))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Продукт или заказ не найден"));
        productOrder.setProduct(null);
        productOrderRepository.save(productOrder);
    }

    @Override
    public void addOrderFromBasket(BasketOrderResponse basketResponse) {
        List<BasketOrderDto> basketDtoList = basketResponse.getData();
        OrderDto orderDto = OrderDto.builder()
                .orderNum(UUID.randomUUID().toString())
                .orderSum(basketDtoList.stream().mapToDouble(BasketOrderDto::getSum).sum())
                .customerId(basketDtoList.stream().map(BasketOrderDto::getCustomerId).findFirst().orElseThrow(() -> new IllegalArgumentException("CustomerId не найден")))
                .build();
        OrderDto savedOrderDto = addOrder(orderDto);

        for (BasketOrderDto basketDto : basketDtoList){
            addProductToOrder(savedOrderDto.getId(), basketDto.getProductId());
            if (basketDto.getCount() > 1){
                changeProductQuantity(savedOrderDto.getId(), basketDto.getProductId(), basketDto.getCount());
            }
        }
    }
}
