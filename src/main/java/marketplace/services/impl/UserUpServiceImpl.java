package marketplace.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import marketplace.models.dto.UserUpFormDto;
import marketplace.models.entities.Product;
import marketplace.models.entities.User;
import marketplace.repositories.RedisRepository;
import marketplace.repositories.UserRepository;
import marketplace.security.filters.JwtProvider;
import marketplace.services.UserUpService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static marketplace.models.dto.UserUpFormDto.from;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserUpServiceImpl implements UserUpService {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RedisRepository redisRepository;
    private final JwtProvider jwtProvider;

    @Override
    public UserUpFormDto getUser(String tokenHeader) {
        User existedUser = new User();
        if (tokenHeader != null && tokenHeader.startsWith("Bearer ")) {
            String token = tokenHeader.substring("Bearer ".length());
            if (!redisRepository.exists(token)) {
                String email = jwtProvider.getLoginFromToken(token);
                Optional<User> optionalUser = userRepository.findUserByEmail(email);
                if (optionalUser.isPresent()) {
                    existedUser = User.builder()
                            .id(optionalUser.get().getId())
                            .email(optionalUser.get().getEmail())
                            .state(optionalUser.get().getState())
                            .firstName(optionalUser.get().getFirstName())
                            .lastName(optionalUser.get().getLastName())
                            .address(optionalUser.get().getAddress())
                            .phone(optionalUser.get().getPhone())
                            .password("")
                            .build();
                    userRepository.save(existedUser);
                } else {
                    throw new IllegalArgumentException("Пользователь с текущим токеном не найден в базе!");
                }
            } else {
                throw new IllegalArgumentException("Токен текущего пользователя в black list!");
            }
        }
        return from(existedUser);
    }

    @Override
    public void updateUser(String tokenHeader, UserUpFormDto userUpForm) {
        User existedUser = new User();
        if (tokenHeader != null && tokenHeader.startsWith("Bearer ")) {
            String token = tokenHeader.substring("Bearer ".length());
            if (!redisRepository.exists(token)) {
                String email = jwtProvider.getLoginFromToken(token);
                Optional<User> optionalUser = userRepository.findUserByEmail(email);
                if (optionalUser.isPresent()) {
                    existedUser = User.builder()
                            .id(optionalUser.get().getId())
                            .email(optionalUser.get().getEmail())
                            .state(optionalUser.get().getState())
                            .firstName(userUpForm.getFirstName())
                            .lastName(userUpForm.getLastName())
                            .address(userUpForm.getAddress())
                            .phone(userUpForm.getPhone())
                            .password(passwordEncoder.encode(userUpForm.getPassword()))
                            .build();
                    userRepository.save(existedUser);
                } else {
                    throw new IllegalArgumentException("Пользователь с текущим токеном не найден в базе!");
                }
            } else {
                throw new IllegalArgumentException("Токен текущего пользователя в блек листе!");
            }
        }
        return;
    }

}
