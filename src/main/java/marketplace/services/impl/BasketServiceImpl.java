package marketplace.services.impl;

import lombok.RequiredArgsConstructor;
import marketplace.models.dto.BasketDto;
import marketplace.models.entities.Basket;
import marketplace.models.dto.BasketOrderDto;
import marketplace.models.dto.ProductDto;
import marketplace.repositories.BasketRepository;
import marketplace.services.BasketService;
import marketplace.services.ProductsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BasketServiceImpl implements BasketService {
    private final BasketRepository basketRepository;
    private final ProductsService productsService;

    @Override
    public List<BasketDto> getBasket(Long customerId) {
        List<Basket> list = new ArrayList<>();
        basketRepository.findBasketDtoByCustomerId(customerId).forEach(list::add);
        return BasketDto.from(list);
    }

    @Override
    public BasketDto addToBasket(Basket basket) {
        if (basket.getCount() == null) {
            basket.setCount(1);
        }
        return BasketDto.from(basketRepository.save(basket));
    }

    @Override
    public void deleteFromBasket(Long basketId) {
        basketRepository.delete(Basket.builder().basketId(basketId).build());
    }

    @Override
    public BasketDto updateBasket(BasketDto basket) {
        Optional<Basket> optBasketDto = basketRepository.findById(basket.getBasketId());
        if (optBasketDto.isPresent()) {
            Basket updatedBasket = optBasketDto.get();
            updatedBasket.setProductId(basket.getProductId());
            updatedBasket.setCount(basket.getCount());
            return BasketDto.from(basketRepository.save(updatedBasket));
        } else {
            throw new IllegalArgumentException("Нет записи с ID " + basket.getBasketId().toString());
        }
    }

    @Override
    public List<BasketOrderDto> getBasketOrderDtoList(Long customerId) {
        List<BasketDto> basketList = getBasket(customerId);
        return basketList.stream().map(basketDto -> {
            ProductDto product = productsService.getProductById(basketDto.getProductId());
            BasketOrderDto basketOrderDto = BasketOrderDto.builder()
                    .basketId(basketDto.getBasketId())
                    .customerId(basketDto.getCustomerId())
                    .productId(basketDto.getProductId())
                    .count(basketDto.getCount())
                    .sum(product.getPrice() * basketDto.getCount())
                    .build();
            deleteFromBasket(basketDto.getBasketId());
            return basketOrderDto;
        }).collect(Collectors.toList());
    }
}
