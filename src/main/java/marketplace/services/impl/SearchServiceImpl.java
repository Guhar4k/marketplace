package marketplace.services.impl;

import java.util.List;
import lombok.RequiredArgsConstructor;
import marketplace.repositories.impl.search.SearchCriteria;
import marketplace.models.dto.ProductDto;
import static marketplace.models.dto.ProductDto.from;
import marketplace.models.dto.SearchDto;
import marketplace.models.entities.Product;
import marketplace.repositories.SearchRepository;
import marketplace.services.SearchService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final SearchRepository searchRepository;

    @Override
    public List<ProductDto> search(SearchDto searchData) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setSearchText(searchData.getText() != null ? "%" + searchData.getText().toLowerCase() + "%" : null);
        searchCriteria.setCategoryId(searchData.getCategory() != null ? searchData.getCategory() : null);
        searchCriteria.setSellerId(searchData.getSeller() != null ? searchData.getSeller() : null);
        searchCriteria.setMinPrice(searchData.getMinPrice() != null ? Double.parseDouble(searchData.getMinPrice()) : 0);
        searchCriteria.setMaxPrice(searchData.getMaxPrice() != null ? Double.parseDouble(searchData.getMaxPrice()) : null);
        searchCriteria.setSorting(searchData.getSorting());
        searchCriteria.setFirstResult((searchData.getPage() - 1) * searchData.getSize());
        searchCriteria.setPageSize(searchData.getSize());

        List<Product> result = searchRepository.findByFilters(searchCriteria);
        return from(result);
    }
}

