package marketplace.services.impl;

import lombok.RequiredArgsConstructor;
import marketplace.models.dto.UserResponse;
import marketplace.models.dto.SignUpForm;
import marketplace.models.entities.Role;
import marketplace.models.entities.User;
import marketplace.models.entities.UserRole;
import marketplace.repositories.RoleRepository;
import marketplace.repositories.UserRepository;
import marketplace.repositories.UserRoleRepository;
import marketplace.services.ConfirmEmailService;
import marketplace.services.SignUpService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static marketplace.models.entities.Role.RoleName.CUSTOMER;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserRoleRepository userRoleRepository;
    private final ConfirmEmailService confirmEmailService;

    @Override
    public UserResponse signUp(SignUpForm signUpForm) {
        if (userRepository.findUserByEmail(signUpForm.getEmail()).isPresent()) {
            throw new IllegalArgumentException("Пользователь с таким email уже зарегистрирован!");
        }
        User user = User.builder()
                .firstName(signUpForm.getFirstName())
                .lastName(signUpForm.getLastName())
                .email(signUpForm.getEmail())
                .password(passwordEncoder.encode(signUpForm.getPassword()))
                .address(signUpForm.getAddress())
                .phone(signUpForm.getPhone())
                .state(User.State.NOT_CONFIRMED)
                .build();
        userRepository.save(user);

        //Добавляем роль для пользователя
        Role.RoleName roleName = getRoleName(signUpForm);

        UserRole userRole = UserRole.builder()
                .role(roleRepository
                        .findRoleByName(roleName)
                        .orElseThrow(() -> new IllegalArgumentException("Роль не найдена")))
                .user(user)
                .build();

        userRoleRepository.save(userRole);

        //Отправка Email
        confirmEmailService.sendEmailConfirmRegistration(user);

        return UserResponse.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .roleName(roleName)
                .build();
    }

    private Role.RoleName getRoleName(SignUpForm signUpForm) {
        Role.RoleName roleName = signUpForm.getRoleName();
        if (roleName == null) {
            roleName = CUSTOMER;
        }

        Optional<Role> optionalRole = roleRepository.findRoleByName(roleName);
        if (optionalRole.isEmpty()) {
            roleRepository.save(Role.builder().name(roleName).build());
        }
        return roleName;
    }
}
