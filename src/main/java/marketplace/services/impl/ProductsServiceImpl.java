package marketplace.services.impl;

import lombok.RequiredArgsConstructor;
import marketplace.models.dto.ProductDto;
import marketplace.models.entities.Product;
import marketplace.repositories.ProductsRepository;
import marketplace.services.ProductsService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static marketplace.models.dto.ProductDto.from;

@Service
@RequiredArgsConstructor
public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    @Override
    public ProductDto addProduct(ProductDto productDto) {
        Product product = Product.builder()
                .name(productDto.getName())
                .description(productDto.getDescription())
                .price(productDto.getPrice())
                .quantity(productDto.getQuantity())
                .photoFile(productDto.getPhotoFile())
                .isDeleted(productDto.getIsDeleted())
                .isVisible(productDto.getIsVisible())
                .build();
        if (productDto.getIsDeleted() == null) product.setIsDeleted(false);
        if (productDto.getIsVisible() == null) product.setIsVisible(true);
        return ProductDto.from(productsRepository.save(product));
    }

    @Override
    public void updateProduct(Long productId, ProductDto productDto) {
        Product product = productsRepository.getById(productId);
        product.setName(productDto.getName());
        product.setDescription(productDto.getDescription());
        product.setPrice(productDto.getPrice());
        product.setQuantity(productDto.getQuantity());
        product.setPhotoFile(productDto.getPhotoFile());
        productsRepository.save(product);
    }

    @Override
    public void deleteProduct(Long productId) {
        Product product = productsRepository.getById(productId);
        product.setIsDeleted(true);
        productsRepository.save(product);
    }

    @Override
    public List<ProductDto> getProductsByCategory(Long categoryId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<Product> result = productsRepository.getProductsByCategoryId(categoryId, pageable);
        return from(result);
    }

    @Override
    public List<ProductDto> getProductsBySellerId(Long sellerId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<Product> result = productsRepository.getProductsBySellerId(sellerId, pageable);
        return from(result);
    }

    @Override
    public void changeProductQuantity(Long productId, Integer quantity) {
        Product product = productsRepository.getById(productId);
        product.setQuantity(quantity);
        productsRepository.save(product);
    }

    @Override
    public List<ProductDto> getAllProducts(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<Product> result = productsRepository.findAllByIsDeletedIsNotAndIsVisibleIs(true, true, pageable);
        return ProductDto.from(result);
    }

    @Override
    public ProductDto getProductById(Long productId) {
        Product product = productsRepository.getById(productId);
        return ProductDto.from(product);
    }
}