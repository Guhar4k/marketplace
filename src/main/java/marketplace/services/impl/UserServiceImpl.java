package marketplace.services.impl;

import lombok.RequiredArgsConstructor;
import marketplace.models.entities.User;
import marketplace.repositories.UserRepository;
import marketplace.services.UserService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public void updateUserStatus(Long userId, User.State state) {
        User user = userRepository.getById(userId);
        user.setState(state);
        userRepository.save(user);
    }
}
