package marketplace.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import marketplace.models.dto.AllUserDto;
import marketplace.models.entities.Customer;
import marketplace.models.entities.Role;
import marketplace.models.entities.Seller;
import marketplace.models.entities.User;
import marketplace.repositories.CustomerRepository;
import marketplace.repositories.SellerRepository;
import marketplace.repositories.UserRepository;
import marketplace.services.UsersService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static marketplace.models.entities.Role.RoleName.CUSTOMER;
import static marketplace.models.entities.Role.RoleName.SELLER;
import static marketplace.models.entities.User.State.DELETED;

@Service
@RequiredArgsConstructor
@Slf4j
public class UsersServiceImpl implements UsersService {
    private final UserRepository usersRepository;
    private final SellerRepository sellersRepository;
    private final CustomerRepository customersRepository;

    @Override
    public List<AllUserDto> findAllSellers(int page, int size) {
        return getAllUserDtos(page, size, SELLER);
    }

    @Override
    public List<AllUserDto> findAllCustomers(int page, int size) {
        return getAllUserDtos(page, size, CUSTOMER);
    }

    private List<AllUserDto> getAllUserDtos(int page, int size, Role.RoleName customer) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by("id"));
        Page<User> result = usersRepository.findAll(pageRequest);
        List<User> usersList = result.getContent().stream()
                .filter(filter -> filter.getUserRoles().stream().iterator().next().getRole().getName().equals(customer)).collect(Collectors.toList());
        return AllUserDto.from(usersList);
    }

    @Override
    public void deleteSellerById(Long id) {
        User user = usersRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Продавец с таким id не найден!"));
        if(user.getUserRoles().stream().iterator().next().getRole().getName().equals(SELLER)) {
            deleteUser(user);
            Seller seller = sellersRepository.findSellerByUser(user);
            seller.setIsDeleted(true);
            sellersRepository.save(seller);
            log.info("Продавец удалён, email = {}", user.getEmail());
        } else {
            throw new IllegalArgumentException("Продавец с таким id не найден");
        }
    }
    @Override
    public void deleteCustomerById(Long id) {
        User user = usersRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Покупатель с таким id не найден!"));
        if(user.getUserRoles().stream().iterator().next().getRole().getName().equals(CUSTOMER)) {
            deleteUser(user);
            Customer customer = customersRepository.findCustomerByUser(user);
            customer.setIsDeleted(true);
            customersRepository.save(customer);
            log.info("Покупатель удалён, email = {}", user.getEmail());
        } else {
            throw new IllegalArgumentException("Покупатель с таким id не найден");
        }
    }
    void deleteUser(User user) {
        user.setState(DELETED);
        usersRepository.save(user);
    }
}
