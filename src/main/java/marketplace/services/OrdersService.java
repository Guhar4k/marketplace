package marketplace.services;

import marketplace.models.dto.*;

import java.util.List;

public interface OrdersService {
    List<OrderDto> getOrders(int page, int size);

    OrderDto addOrder(OrderDto orderDto);

    OrderDto updateOrder(Long orderId, OrderDto orderDto);

    void deleteOrder(Long orderId);

    DeliveryDto addDeliveryToOrder(Long orderId, DeliveryDto deliveryDto);

    List<OrderDto> getOrdersByUser(Long customerId, int page, int size);

    OrderDto getOrderById(Long orderId);

    List<ProductDto> getProductsByOrderId(Long orderId);

    OrderStatusDto getOrderStatus(Long orderId);

    DeliveryDto getDelivery(Long orderId);

    List<ProductDto> addProductToOrder(Long orderId, Long productId);

    List<ProductDto> changeProductQuantity(Long orderId, Long productId, Integer quantity);

    void deleteProductInOrder(Long orderId, Long productId);

    void addOrderFromBasket(BasketOrderResponse basketResponse);
}
