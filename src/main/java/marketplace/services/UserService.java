package marketplace.services;

import marketplace.models.entities.User;

public interface UserService {
    void updateUserStatus(Long userId, User.State state);
}
