package marketplace.services;

import marketplace.models.dto.UserResponse;
import marketplace.models.dto.SignUpForm;

public interface SignUpService {
    UserResponse signUp(SignUpForm signUpForm);
}
