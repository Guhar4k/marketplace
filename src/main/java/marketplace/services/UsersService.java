package marketplace.services;

import marketplace.models.dto.AllUserDto;

import java.util.List;

public interface UsersService {
    List<AllUserDto> findAllSellers(int page, int size);
    List<AllUserDto> findAllCustomers(int page, int size);
    void deleteSellerById(Long id);
    void deleteCustomerById(Long id);
}
