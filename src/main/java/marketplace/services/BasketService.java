package marketplace.services;

import marketplace.models.dto.BasketDto;
import marketplace.models.entities.Basket;
import marketplace.models.dto.BasketOrderDto;

import java.util.List;

public interface BasketService {
    List<BasketDto> getBasket(Long customerId);

    BasketDto addToBasket(Basket basket);

    void deleteFromBasket(Long basketId);

    BasketDto updateBasket(BasketDto basket);

    List<BasketOrderDto> getBasketOrderDtoList(Long customerId);
}
