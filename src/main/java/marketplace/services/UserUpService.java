package marketplace.services;

import marketplace.models.dto.ProductDto;
import marketplace.models.dto.UserUpFormDto;

public interface UserUpService {
    UserUpFormDto getUser(String tokenHeader);
    void updateUser(String tokenHeader, UserUpFormDto userUpForm);
}
