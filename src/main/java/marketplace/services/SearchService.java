package marketplace.services;

import java.util.List;
import marketplace.models.dto.ProductDto;
import marketplace.models.dto.SearchDto;

public interface SearchService {
    List<ProductDto> search(SearchDto searchData);
}
