package marketplace.services;

import marketplace.models.dto.ProductDto;
import marketplace.models.entities.Product;

import java.util.List;

public interface ProductsService {

    List<ProductDto> getProductsByCategory(Long categoryId, int page, int size);

    List<ProductDto> getProductsBySellerId(Long sellerId, int page, int size);

    void changeProductQuantity(Long productId, Integer quantity);

    List<ProductDto> getAllProducts(int page, int size);

    ProductDto addProduct(ProductDto productDto);

    ProductDto getProductById(Long productId);

    void updateProduct(Long productId, ProductDto productDto);

    void deleteProduct(Long productId);
}
