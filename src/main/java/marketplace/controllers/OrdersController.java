package marketplace.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import marketplace.models.dto.*;
import org.springframework.http.ResponseEntity;

@Tag(
        name = "Контроллер для работы с заказами",
        description = "Контроллер показывает информацию по заказу, добавляет/обновляет/удаляет заказ," +
                "а также добавляет продукты в заказ, меняет их количество и удаляет их из заказа"
)
public interface OrdersController {

    @Operation(
            summary = "Получить все заказы",
            description = "Позволяет получить заказы с помощью пагинации"
    )
    ResponseEntity<OrderResponse> getOrders (int page,int size);

    @Operation(
            summary = "Получить все заказы конкретного пользователя",
            description = "Позволяет получить все заказы по id пользователя с помощью пагинации"
    )
    ResponseEntity<OrderResponse> getOrdersByUser (Long customerId, int page, int size);

    @Operation(
            summary = "Получить конкретный заказ",
            description = "Позволяет получить заказ по id"
    )
    ResponseEntity<OrderDto> getOrderById(Long orderId);

    @Operation(
            summary = "Получить все продукты в заказе",
            description = "Позволяет получить все продукты в заказе по id заказа"
    )
    ResponseEntity<ProductsResponse> getProductsByOrderId(Long orderId);

    @Operation(
            summary = "Получить всю информацию о заказе",
            description = "Позволяет получить всю информацию о заказе по id заказа"
    )
    ResponseEntity<ProductOrderResponse> getFullOrderInfoByOrderId(Long orderId);

    @Operation(
            summary = "Добавление заказа",
            description = "Добавление заказа в БД"
    )
    OrderDto addOrder(OrderDto orderDto);

    @Operation(
            summary = "Обновление заказа",
            description = "Обновление данных в заказе по id заказа"
    )
    OrderDto updateOrder(Long orderId, OrderDto orderDto);

    @Operation(
            summary = "Обновление доставки",
            description = "Позволяет назначить определенную компанию для доставки заказа по id заказа и названию доставки"
    )
    DeliveryDto updateDeliveryInOrder(Long orderId, DeliveryDto deliveryDto);

    @Operation(
            summary = "Добавление продукта в заказ",
            description = "Позволяет добавить продукт, которого еще не было в заказе по id заказа и по id продукта"
    )
    ProductsResponse addProductToOrder(Long orderId, Long productId);

    @Operation(
            summary = "Обновление количества продукта в заказе",
            description = "Позволяет изменить количество продукта в заказе в зависимости от наличия продукта на складе. " +
                    "Нужно указать id заказа, id продукта, и нужное количество"
    )
    ProductsResponse changeProductQuantity(Long orderId, Long productId, Integer quantity);

    @Operation(
            summary = "Удаление продукта из заказа",
            description = "Позволяет удалить продукт из заказа по id заказа и по id продукта"
    )
    void deleteProductInOrder(Long orderId, Long productId);

    @Operation(
            summary = "Удаление заказа",
            description = "Устанавливет статус DELETED конкретному заказу по id заказа"
    )
    void deleteOrder(Long orderId);

    @Operation(
            summary = "Формирование заказа из корзины",
            description = "Сохраняет заказ по данным из корзины(связывает данные в таблицах order, product_order, customer, product)"
    )
    void addOrderFromBasket(BasketOrderResponse basketResponse);
}
