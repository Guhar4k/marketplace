package marketplace.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import marketplace.models.dto.ProductsResponse;
import marketplace.models.dto.SearchDto;
import org.springframework.http.ResponseEntity;

@Tag(
        name = "Контроллер для поиска товаров по критериям",
        description = "Контроллер возвращает товары по заданным параметрам"
)
public interface SearchController {

    @Operation(
            summary = "Получить товары по критериям",
            description = "Позволяет получить товары по критериям и отсортировать"
    )
    ResponseEntity<ProductsResponse> search(SearchDto searchData);
}
