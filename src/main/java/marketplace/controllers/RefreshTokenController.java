package marketplace.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import marketplace.models.dto.TokenRefreshDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

@Tag(
        name = "Контроллер для обновления access_token и refresh_token"
)
public interface RefreshTokenController {
    @Operation(
            summary = "Получает от клиента refresh_token, " +
                    "в ответ отдает пару с новым access_token и текущим refresh_token",
            description = "Осуществляет проверку валидности refresh_token, в " +
                    "в случае успешности отдает новую пару токенов"
    )
    ResponseEntity<?> refreshToken(@RequestBody TokenRefreshDto tokenRefreshDto);
}
