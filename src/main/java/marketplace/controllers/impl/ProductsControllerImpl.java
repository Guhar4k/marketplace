package marketplace.controllers.impl;

import lombok.RequiredArgsConstructor;
import marketplace.controllers.ProductsController;
import marketplace.models.dto.ProductDto;
import marketplace.models.dto.ProductsResponse;
import marketplace.services.ProductsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
public class ProductsControllerImpl implements ProductsController {
    private final ProductsService productsService;

    @Override
    @GetMapping
    public ResponseEntity<ProductsResponse> getAllProducts(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .body(ProductsResponse.builder().data(productsService.getAllProducts(page, size)).build());
    }

    @Override
    @GetMapping(value = "/category-{category-id}")
    public ResponseEntity<ProductsResponse> getProductsByCategory(@PathVariable("category-id") Long categoryId,
                                                                  @RequestParam("page") int page,
                                                                  @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .body(ProductsResponse.builder()
                        .data(productsService.getProductsByCategory(categoryId, page, size))
                        .build());
    }

    @Override
    @GetMapping(value = "/{product-id}")
    public ResponseEntity<ProductDto> getProductById(@PathVariable("product-id") Long productId) {
        return ResponseEntity.ok()
                .body(productsService.getProductById(productId));
    }

    @Override
    @GetMapping(value = "/seller-{seller-id}")
    public ResponseEntity<ProductsResponse> getProductsBySellerId(@PathVariable("seller-id") Long sellerId,
                                                                  @RequestParam("page") int page,
                                                                  @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .body(ProductsResponse.builder()
                        .data(productsService.getProductsBySellerId(sellerId, page, size))
                        .build());
    }

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProductDto addProduct(@RequestBody ProductDto productDto) {
        return productsService.addProduct(productDto);
    }

    @Override
    @PutMapping(value = "/{product-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void updateProduct(@PathVariable("product-id") Long productId,
                              @RequestBody ProductDto productDto) {
        productsService.updateProduct(productId, productDto);
    }

    @Override
    @PutMapping(value = "/{product-id}/{quantity}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void changeProductQuantity(@PathVariable("product-id") Long productId,
                                      @PathVariable("quantity") Integer quantity) {
        productsService.changeProductQuantity(productId, quantity);
    }

    @Override
    @DeleteMapping(value = "/{product-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteProduct(@PathVariable("product-id") Long productId) {
        productsService.deleteProduct(productId);
    }
}
