package marketplace.controllers.impl;

import lombok.RequiredArgsConstructor;
import marketplace.controllers.BasketController;
import marketplace.models.dto.*;
import marketplace.models.entities.Basket;
import marketplace.services.BasketService;
import marketplace.services.OrdersService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/basket")
@RequiredArgsConstructor
public class BasketControllerImpl implements BasketController {
    private final BasketService basketService;
    private final OrdersService ordersService;

    @GetMapping(value = "/customer/{customer-id}")
    public ResponseEntity<BasketResponseDto> getBasket(@PathVariable("customer-id") Long customerId) {
        return ResponseEntity.ok().body(BasketResponseDto.builder().data(basketService.getBasket(customerId)).build());
    }

    @PutMapping(value = "/customer/{customer-id}/product/{product-id}/count/{products-count}")
    @ResponseStatus(HttpStatus.CREATED)
    public BasketDto addBasket(@PathVariable("customer-id") Long customerId, @PathVariable("product-id") Long productId, @PathVariable(name = "products-count") Integer count) {
        return basketService.addToBasket(Basket.builder()
                .customerId(customerId)
                .productId(productId)
                .count(count).build());
    }

    @PutMapping(value = "/customer/{customer-id}/product/{product-id}")
    @ResponseStatus(HttpStatus.CREATED)
    public BasketDto addBasket(@PathVariable("customer-id") Long customerId, @PathVariable("product-id") Long productId) {
        return basketService.addToBasket(Basket.builder()
                .customerId(customerId)
                .productId(productId).build());
    }

    @PostMapping(value = "/customer/{customer-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public BasketDto updateBasket(@PathVariable("customer-id") Long customerId, @RequestBody BasketDto basket) {
        return basketService.updateBasket(basket);
    }

    @DeleteMapping(value = "/customer/{customer-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteBasket(@PathVariable("customer-id") Long customerId, @RequestBody BasketDto basket) {
        basketService.deleteFromBasket(basket.getBasketId());
    }

    @PostMapping(value = "/customer/{customer-id}/order")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void makeOrder(@PathVariable("customer-id") Long customerId){
        List<BasketOrderDto> basketProducts = basketService.getBasketOrderDtoList(customerId);
        ordersService.addOrderFromBasket(new BasketOrderResponse(basketProducts));
    }
}
