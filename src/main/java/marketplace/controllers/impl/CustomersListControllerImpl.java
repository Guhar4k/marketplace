package marketplace.controllers.impl;

import lombok.RequiredArgsConstructor;
import marketplace.controllers.CustomersListController;
import marketplace.models.dto.AllUsersResponse;
import marketplace.services.UsersService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/customers")
@RequiredArgsConstructor
public class CustomersListControllerImpl implements CustomersListController {
    private final UsersService usersService;
    @GetMapping
    public ResponseEntity<AllUsersResponse> getCustomers(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDate.now().toString()))
                .body(AllUsersResponse.builder().data(usersService.findAllCustomers(page, size)).build());
    }

    @Override
    @DeleteMapping(value = "/{user-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteCustomer(@PathVariable("user-id") Long userId) {
        usersService.deleteCustomerById(userId);
    }

}
