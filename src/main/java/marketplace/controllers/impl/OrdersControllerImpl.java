package marketplace.controllers.impl;

import lombok.RequiredArgsConstructor;
import marketplace.controllers.OrdersController;
import marketplace.models.dto.*;
import marketplace.services.OrdersService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
public class OrdersControllerImpl implements OrdersController {
    private final OrdersService ordersService;

    @GetMapping
    public ResponseEntity<OrderResponse> getOrders (@RequestParam("page") int page,
                                                    @RequestParam("size") int size){
        return ResponseEntity.ok()
                .body(OrderResponse.builder().data(ordersService.getOrders(page,size)).build());
    }

    @GetMapping(value = "/customer/{customer-id}")
    public ResponseEntity<OrderResponse> getOrdersByUser (@PathVariable("customer-id") Long customerId, @RequestParam("page") int page,
                                                    @RequestParam("size") int size){
        return ResponseEntity.ok()
                .body(OrderResponse.builder()
                        .data(ordersService.getOrdersByUser(customerId, page, size))
                        .build());
    }

    @GetMapping(value = "/{order-id}")
    public ResponseEntity<OrderDto> getOrderById(@PathVariable("order-id") Long orderId){
        return ResponseEntity.ok()
                .body(ordersService.getOrderById(orderId));
    }

    @GetMapping(value = "/{order-id}/products")
    public ResponseEntity<ProductsResponse> getProductsByOrderId(@PathVariable("order-id") Long orderId){
        return ResponseEntity.ok()
                .body(ProductsResponse.builder()
                        .data(ordersService.getProductsByOrderId(orderId))
                        .build()
                        );
    }

    @GetMapping(value = "/{order-id}/full-order-info")
    public ResponseEntity<ProductOrderResponse> getFullOrderInfoByOrderId(@PathVariable("order-id") Long orderId){
        return ResponseEntity.ok()
                .body(ProductOrderResponse.builder()
                        .orderDto(ordersService.getOrderById(orderId))
                        .orderStatusDto(ordersService.getOrderStatus(orderId))
                        .deliveryDto(ordersService.getDelivery(orderId))
                        .products(ordersService.getProductsByOrderId(orderId))
                        .build()
                );
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OrderDto addOrder(@RequestBody OrderDto orderDto){
        return ordersService.addOrder(orderDto);
    }

    @PutMapping(value = "/{order-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public OrderDto updateOrder(@PathVariable("order-id") Long orderId, @RequestBody OrderDto orderDto){
        return ordersService.updateOrder(orderId, orderDto);
    }

    @PutMapping(value = "/{order-id}/update-delivery")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public DeliveryDto updateDeliveryInOrder(@PathVariable("order-id") Long orderId, @RequestBody DeliveryDto deliveryDto) {
        return ordersService.addDeliveryToOrder(orderId, deliveryDto);
    }

    @PostMapping(value = "/{order-id}/products/{product-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ProductsResponse addProductToOrder(@PathVariable("order-id") Long orderId,
                                              @PathVariable("product-id") Long productId){
        return ProductsResponse.builder()
                .data(ordersService.addProductToOrder(orderId, productId))
                .build();
    }

    @PutMapping(value = "/{order-id}/products/{product-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ProductsResponse changeProductQuantity(@PathVariable("order-id") Long orderId,
                                                  @PathVariable("product-id") Long productId,
                                                  @RequestParam Integer quantity){
        return ProductsResponse.builder()
                .data(ordersService.changeProductQuantity(orderId, productId, quantity))
                .build();
    }

    @DeleteMapping(value = "/{order-id}/products/{product-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteProductInOrder(@PathVariable("order-id") Long orderId,
                                     @PathVariable("product-id") Long productId){
        ordersService.deleteProductInOrder(orderId, productId);
    }


    @DeleteMapping(value = "/{order-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteOrder(@PathVariable("order-id") Long orderId){
        ordersService.deleteOrder(orderId);
    }

    @PostMapping(value = "/basket")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void addOrderFromBasket(@RequestBody BasketOrderResponse basketResponse){
        ordersService.addOrderFromBasket(basketResponse);
    }
}
