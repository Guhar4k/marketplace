package marketplace.controllers.impl;

import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import marketplace.controllers.SearchController;
import marketplace.models.dto.ProductsResponse;
import marketplace.models.dto.SearchDto;
import marketplace.services.SearchService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/search")
@RequiredArgsConstructor
public class SearchControllerImpl implements SearchController {
    private final SearchService searchService;

    @GetMapping
    public ResponseEntity<ProductsResponse> search(@Valid SearchDto searchData) {
        return ResponseEntity.ok()
            .body(ProductsResponse.builder().data(searchService.search(searchData)).build());
    }
}
