package marketplace.controllers.impl;

import lombok.RequiredArgsConstructor;
import marketplace.controllers.SignUpController;
import marketplace.models.dto.SignUpForm;
import marketplace.services.ConfirmEmailService;
import marketplace.services.SignUpService;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/signUp")
public class SignUpControllerImpl implements SignUpController {
    private final SignUpService signUpService;
    private final ConfirmEmailService confirmEmailService;

    @Override
    @PostMapping
    public ResponseEntity<Object> signUp(@Valid  @RequestBody SignUpForm form, BindingResult result) {
        if (result.hasErrors()) {
            Map<String, List<String>> resultErrors = new HashMap<>();
            resultErrors.put("errors", result.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList()));
            return ResponseEntity.badRequest().body(resultErrors);
        }
        try {
            return ResponseEntity.ok(signUpService.signUp(form));
        } catch (IllegalArgumentException e) {
            Map<String, List<String>> resultErrors = new HashMap<>();
            if (e.getMessage() != null) {
                resultErrors.put("errors", List.of(e.getMessage()));
            }
            return ResponseEntity.badRequest().body(resultErrors);
        }
    }

    @GetMapping("/registrationConfirm/{token}")
    public ResponseEntity<Object> registrationConfirm(@PathVariable("token") String token) {
        if (confirmEmailService.checkRegistrationToken(token)) {
            return ResponseEntity.ok("Email подтвержден");
        } else {
            Map<String, List<String>> resultErrors = new HashMap<>();
            resultErrors.put("errors", List.of("Возникла ошибка!"));
            return ResponseEntity.badRequest().body(resultErrors);
        }
    }
}
