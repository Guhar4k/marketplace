package marketplace.controllers.impl;

import lombok.RequiredArgsConstructor;
import marketplace.controllers.UserUpController;
import marketplace.models.dto.UserUpFormDto;
import marketplace.services.UserUpService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/userPage")
@RequiredArgsConstructor
public class UserUpControllerImpl implements UserUpController {
    private final UserUpService userUpService;


    @GetMapping
    @Override
    public ResponseEntity<UserUpFormDto> getUser(@RequestHeader (name="Authorization") String tokenHeader) {

        return ResponseEntity.ok()
                .body(userUpService.getUser(tokenHeader));
    }

    @PutMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    @Override
    public void updateUser(@Valid @RequestBody UserUpFormDto user, @RequestHeader (name="Authorization") String tokenHeader) {
        userUpService.updateUser(tokenHeader, user);
    }
}
