package marketplace.controllers.impl;

import lombok.RequiredArgsConstructor;
import marketplace.controllers.RefreshTokenController;
import marketplace.models.dto.TokenResponse;
import marketplace.models.dto.TokenRefreshDto;
import marketplace.models.entities.RefreshToken;
import marketplace.security.filters.JwtProvider;
import marketplace.services.RefreshTokenService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/refreshToken")
@RequiredArgsConstructor
public class RefreshTokenControllerImpl implements RefreshTokenController {
    private final RefreshTokenService refreshTokenService;
    private final JwtProvider jwtProvider;

    @PostMapping
    public ResponseEntity<?> refreshToken(@RequestBody TokenRefreshDto tokenRefreshDto) {
        String refreshToken = tokenRefreshDto.getRefreshToken();
        return refreshTokenService.findByToken(refreshToken)
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(account -> {
                    String accessToken = jwtProvider.generateToken(account);
                    return ResponseEntity.ok(TokenResponse.builder()
                            .accessToken(accessToken)
                            .refreshToken(refreshToken)
                            .build());
                })
                .orElseThrow(() -> new IllegalArgumentException("refresh_token не валидный!"));

    }
}
