package marketplace.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import marketplace.models.dto.BasketDto;
import marketplace.models.entities.Basket;
import marketplace.models.dto.BasketResponseDto;
import org.springframework.http.ResponseEntity;

@Tag(
        name = "Контроллер для работы с корзиной",
        description = "Корзина - временный выбор товара перед заказом"
)
public interface BasketController {

    @Operation(
            summary = "Получение корзины",
            description = "Получение списка товаров из корзины заданного покупателя"
    )
    ResponseEntity<BasketResponseDto> getBasket(Long customerId);

    @Operation(
            summary = "Добавление товара в корзину",
            description = "Добавление товара в корзину заданного покупателя с указанием количества"
    )
    BasketDto addBasket(Long customerId, Long productId, Integer count);


    @Operation(
            summary = "Добавление одного товара в корзину",
            description = "Добавление товара в корзину заданного покупателя без указания количества"
    )
    BasketDto addBasket(Long customerId, Long productId);

    @Operation(
            summary = "Обновление корзины",
            description = "Обновление записи товара в корзине заданного покупателя"
    )
    BasketDto updateBasket(Long customerId, BasketDto basket);

    @Operation(
            summary = "Удаление товара из корзины",
            description = "Удаление записи товара из корзины заданного покупателя"
    )
    void deleteBasket(Long customerId, BasketDto basket);

    @Operation(
            summary = "Создание заказа по товарам из корзины",
            description = "Создание заказа по товарам из корзины заданного покупателя"
    )
    void makeOrder(Long customerId);
}
