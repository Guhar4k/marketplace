package marketplace.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import marketplace.models.dto.AllUsersResponse;
import org.springframework.http.ResponseEntity;


@Tag(
        name = "Контроллер для получения списка продавцов и их удаление",
        description = "Получение списка продавцов и их удаление"
)
public interface SellersListController {
    @Operation(
            summary = "Получение списка продавцов",
            description = "Позволяет получить список продавцов"
    )
    ResponseEntity<AllUsersResponse> getSellers(int page, int size);

    @Operation(
            summary = "Удаление продавцов по его id",
            description = "Позволяет удалить продавца по id"
    )
    void deleteSeller(Long userId);
}
