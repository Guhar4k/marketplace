package marketplace.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import marketplace.models.dto.UserUpFormDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.validation.Valid;

@Tag(
                name = "Контроллер для редактирования данных пользователя",
                description = "Редактирование пользователем своих данных"
        )
public interface UserUpController {
    @Operation(
                        summary = "Получить персональные данные пользователя",
                        description = "Позволяет получить данные текущего авторизированного пользователя"
                )
    ResponseEntity<UserUpFormDto> getUser(String tokenHeader);

    @Operation(
            summary = "Корректировать персональные данные пользователя",
            description = "Позволяет сохранить данные текущего авторизированного пользователя в базу"
    )
    void updateUser(@Valid UserUpFormDto user, String tokenHeader);
}
