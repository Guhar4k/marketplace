package marketplace.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import marketplace.models.dto.SignUpForm;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;

@Tag(
        name = "Контроллер для регистрации новых пользователей"
)
public interface SignUpController {
    @Operation(
            summary = "Первоначальный ввод данных о пользователе.",
            description = "Осуществляет проверку валидности полей. " +
                    "Добавляет пользователя в базу данных. Отправляет email для подтверждения."
    )
    ResponseEntity<Object> signUp(@Valid SignUpForm form, BindingResult result);
    @Operation(
            summary = "Подтверждение email по токену",
            description = "Проверка токена с имеющимся в базе. Если токен найден, " +
                    "то статус пользователя меняется на CONFIRMED"
    )
    ResponseEntity<Object> registrationConfirm(@PathVariable("token") String token);
}
