package marketplace.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import marketplace.models.dto.ProductDto;
import marketplace.models.dto.ProductsResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Tag(
        name = "Контроллер для работы с товарами",
        description = "Контроллер показывает информацию по товарам, добавляет/обновляет/удаляет товары," +
                "а также просмотр товаров по категориям и продавцам"
)
public interface ProductsController {

    @Operation(
            summary = "Получить все товары",
            description = "Позволяет получить товары с помощью пагинации"
    )
    ResponseEntity<ProductsResponse> getAllProducts(@RequestParam("page") int page,
                                                    @RequestParam("size") int size);

    @Operation(
            summary = "Получить все товары в категории",
            description = "Позволяет получить все товары в категории по id категории"
    )
    ResponseEntity<ProductsResponse> getProductsByCategory(@PathVariable("category-id") Long categoryId,
                                                           @RequestParam("page") int page,
                                                           @RequestParam("size") int size);

    @Operation(
            summary = "Получить конкретный товар",
            description = "Позволяет получить товар по id"
    )
    ResponseEntity<ProductDto> getProductById(@PathVariable("product-id") Long productId);

    @Operation(
            summary = "Получить все товары конкретного магазина",
            description = "Позволяет получить все товары по id магазина с помощью пагинации"
    )
    ResponseEntity<ProductsResponse> getProductsBySellerId(@PathVariable("seller-id") Long sellerId,
                                                           @RequestParam("page") int page,
                                                           @RequestParam("size") int size);

    @Operation(
            summary = "Добавление товара",
            description = "Добавление товара в БД"
    )
    ProductDto addProduct(@RequestBody ProductDto productDto);

    @Operation(
            summary = "Обновление товара",
            description = "Обновление данных в товаре по id товара"
    )
    void updateProduct(@PathVariable("product-id") Long productId,
                       @RequestBody ProductDto productDto);

    @Operation(
            summary = "Обновление количества товара",
            description = "Позволяет изменить количество товара в зависимости от наличия продукта на складе. " +
                    "Нужно указать id товара, и нужное количество"
    )
    void changeProductQuantity(@PathVariable("product-id") Long productId,
                               @PathVariable("quantity") Integer quantity);

    @Operation(
            summary = "Удаление товара",
            description = "Устанавливет статус DELETED конкретному товару по id товара"
    )
    void deleteProduct(@PathVariable("product-id") Long productId);
}