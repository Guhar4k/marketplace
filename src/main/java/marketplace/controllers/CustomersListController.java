package marketplace.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import marketplace.models.dto.AllUsersResponse;
import org.springframework.http.ResponseEntity;


@Tag(
        name = "Контроллер для получения списка покупателей и их удаление",
        description = "Получение списка покупателей и их удаление"
)
public interface CustomersListController {
    @Operation(
            summary = "Получение списка покупателей",
            description = "Позволяет получить список покупателей"
    )
    ResponseEntity<AllUsersResponse> getCustomers(int page, int size);

    @Operation(
            summary = "Удаление покупателя по его id",
            description = "Позволяет удалить покупателя по id"
    )
    void deleteCustomer(Long userId);
}

