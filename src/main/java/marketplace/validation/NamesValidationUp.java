package marketplace.validation;

import marketplace.models.dto.UserUpFormDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NamesValidationUp implements ConstraintValidator<NotSameFieldsUp, UserUpFormDto> {
    @Override
    public boolean isValid(UserUpFormDto userUpForm, ConstraintValidatorContext constraintValidatorContext) {
        return !userUpForm.getFirstName().equals(userUpForm.getLastName());
    }
}
