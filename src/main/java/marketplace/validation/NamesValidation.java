package marketplace.validation;

import marketplace.models.dto.SignUpForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NamesValidation implements ConstraintValidator<NotSameFields, SignUpForm> {
    @Override
    public boolean isValid(SignUpForm signUpForm, ConstraintValidatorContext constraintValidatorContext) {
        return !signUpForm.getFirstName().equals(signUpForm.getLastName());
    }
}
