package marketplace.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = NamesValidation.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface NotSameFields {
    String message() default "Поля <firstName> и <lastName> не должны совпадать!";

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default  {};
}
