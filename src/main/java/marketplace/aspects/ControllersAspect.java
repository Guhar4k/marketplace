package marketplace.aspects;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import marketplace.models.dto.BasketResponseDto;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

@Component
@Aspect
@Slf4j
@RequiredArgsConstructor
public class ControllersAspect {

    @Around(value = "execution(void marketplace.controllers.*.*(..))")
    public void logVoidTypeMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        long before = System.currentTimeMillis();
        joinPoint.proceed();
        long after = System.currentTimeMillis();
        String name = joinPoint.getSignature().getName();
        log.info("Название метода: " + name + ", время выполнения " + String.valueOf(after - before) +
                "мс, дата: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")).toString());
    }

    @Around(value = "execution((org.springframework.http.ResponseEntity) marketplace.controllers.*.*(..))")
    public Object logResponseEntityMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        return makeLogInfo(joinPoint);
    }

    @Around(value = "execution((marketplace.models.dto.OrderDto) marketplace.controllers.*.*(..))")
    public Object logOrderDtoMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        return makeLogInfo(joinPoint);
    }

    @Around(value = "execution((marketplace.models.dto.BasketDto) marketplace.controllers.*.*(..))")
    public Object logBasketDtoMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        return makeLogInfo(joinPoint);
    }

    @Around(value = "execution((marketplace.models.dto.DeliveryDto) marketplace.controllers.*.*(..))")
    public Object logDeliveryDtoMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        return makeLogInfo(joinPoint);
    }

    @Around(value = "execution((marketplace.models.dto.ProductsResponse) marketplace.controllers.*.*(..))")
    public Object logProductsResponseMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        return makeLogInfo(joinPoint);
    }

    @Around(value = "execution((marketplace.models.dto.ProductDto) marketplace.controllers.*.*(..))")
    public Object logProductDtoMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        return makeLogInfo(joinPoint);
    }

    @Around(value = "execution((marketplace.models.dto.UserUpFormDto) marketplace.controllers.*.*(..))")
    public Object logUserUpFormDtoMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        return makeLogInfo(joinPoint);
    }

    private Object makeLogInfo(ProceedingJoinPoint joinPoint) throws Throwable {
        long before = System.currentTimeMillis();
        Object response = joinPoint.proceed();
        long after = System.currentTimeMillis();
        String name = joinPoint.getSignature().getName();
        log.info("Название метода: " + name + ", время выполнения " + (after - before) +
                "мс, дата: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
        return response;
    }
}
