package marketplace.security.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import marketplace.models.entities.RefreshToken;
import marketplace.models.dto.TokenResponse;
import marketplace.models.dto.SignInForm;
import marketplace.models.entities.User;
import marketplace.security.details.AccountUserDetails;
import marketplace.services.RefreshTokenService;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TokenAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final JwtProvider jwtProvider;
    private final RefreshTokenService refreshTokenService;

    public TokenAuthenticationFilter(AuthenticationManager authenticationManager,
                                     JwtProvider jwtProvider, RefreshTokenService refreshTokenService) {
        super(authenticationManager);
        this.jwtProvider = jwtProvider;
        this.refreshTokenService = refreshTokenService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            SignInForm form = objectMapper.readValue(request.getReader(), SignInForm.class);
            UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(form.getEmail(), form.getPassword());
            return super.getAuthenticationManager().authenticate(token);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException {
        AccountUserDetails userDetails = (AccountUserDetails) authResult.getPrincipal();
        User user = userDetails.getUser();
        String accessToken = jwtProvider.generateToken(user);
        RefreshToken refreshToken = refreshTokenService.createRefreshToken(user.getId());
        ObjectMapper objectMapper = new ObjectMapper();
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        objectMapper.writeValue(response.getWriter(), TokenResponse.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken.getToken())
                .build());
    }
}
