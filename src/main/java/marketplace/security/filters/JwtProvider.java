package marketplace.security.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import marketplace.models.entities.User;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class JwtProvider {
    public static final String SECRET_CODE = "GoodDay";
    public static final Integer EXPIRED_TIME_MINUTE = 60 * 24;

    public String generateToken(User user) {
        Date date = Date.from(LocalDateTime
                .now()
                .plusMinutes(EXPIRED_TIME_MINUTE)
                .atZone(ZoneId.systemDefault())
                .toInstant());
        return JWT.create()
                .withExpiresAt(date)
                .withSubject(user.getId().toString())
                .withClaim("email", user.getEmail())
                .withClaim("state", user.getState().toString())
                .sign(Algorithm.HMAC256(SECRET_CODE));
    }

    public String getLoginFromToken(String token) {
        try {
            DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC256(SECRET_CODE))
                    .build().verify(token);
            return decodedJWT.getClaim("email").asString();
        } catch (JWTVerificationException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
