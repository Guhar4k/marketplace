package marketplace.security.filters;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import marketplace.models.entities.User;
import marketplace.repositories.RedisRepository;
import marketplace.repositories.UserRepository;
import marketplace.security.config.SecurityConfig;
import marketplace.security.details.AccountUserDetails;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
public class TokenAuthorizationFilter extends OncePerRequestFilter {
    private final UserRepository userRepository;
    private final RedisRepository redisRepository;
    private final JwtProvider jwtProvider;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (!request.getRequestURI().equals(SecurityConfig.LOGIN_FILTER_PROCESSES_URL)
                && !request.getRequestURI().equals(SecurityConfig.LOGOUT_FILTER_PROCESSES_URL)) {
            String tokenHeader = request.getHeader("Authorization");
            if (tokenHeader != null && tokenHeader.startsWith("Bearer ")) {
                String token = tokenHeader.substring("Bearer ".length());
                if (!redisRepository.exists(token)) {
                    String email = jwtProvider.getLoginFromToken(token);
                    Optional<User> optionalUser = userRepository.findUserByEmail(email);
                    if (optionalUser.isPresent()) {
                        AccountUserDetails userDetails = new AccountUserDetails(optionalUser.get());
                        UsernamePasswordAuthenticationToken authenticationToken =
                                new UsernamePasswordAuthenticationToken(token, null, userDetails.getAuthorities());
                        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                    } else {
                        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        ObjectMapper objectMapper = new ObjectMapper();
                        Map<String, List<String>> resultErrors = new HashMap<>();
                        resultErrors.put("errors", List.of("Пользователь не найден!"));
                        objectMapper.writeValue(response.getWriter(), resultErrors);
                        return;
                    }
                }
            }
        }
        filterChain.doFilter(request, response);
    }
}
