package marketplace.security.config;

import marketplace.repositories.RedisRepository;
import marketplace.repositories.UserRepository;
import marketplace.security.filters.JwtProvider;
import marketplace.security.filters.TokenAuthenticationFilter;
import marketplace.security.filters.TokenAuthorizationFilter;
import marketplace.security.filters.TokenLogoutFilter;
import marketplace.services.RefreshTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String LOGIN_FILTER_PROCESSES_URL = "/api/login";
    public static final String LOGOUT_FILTER_PROCESSES_URL = "/api/logout";
    public static final String PRODUCTS_API = "/api/products";
    public static final String USER_PAGE_URL = "/api/userPage";
    public static final String ORDERS_API = "/api/orders";
    public static final String BASKET_API = "/api/basket";
    public static final String CUSTOMER_LIST_API = "/api/customers";
    public static final String SELLERS_LIST_API = "/api/sellers";

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService accountUserDetailsService;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private RefreshTokenService refreshTokenService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RedisRepository redisRepository;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(accountUserDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        TokenAuthenticationFilter tokenAuthenticationFilter =
                new TokenAuthenticationFilter(authenticationManagerBean(), jwtProvider, refreshTokenService);
        tokenAuthenticationFilter.setFilterProcessesUrl(LOGIN_FILTER_PROCESSES_URL);

        http
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
                .addFilter(tokenAuthenticationFilter)
                .addFilterBefore(new TokenAuthorizationFilter(userRepository, redisRepository, jwtProvider),
                UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(new TokenLogoutFilter(redisRepository), TokenAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(LOGIN_FILTER_PROCESSES_URL + "/**").permitAll()
                .antMatchers(LOGOUT_FILTER_PROCESSES_URL + "/**").hasAnyAuthority()
                .antMatchers("/api/signUp/**").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/api/refreshToken").hasAnyAuthority()
                .antMatchers(USER_PAGE_URL).hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, PRODUCTS_API + "/**").permitAll()
                .antMatchers(HttpMethod.POST, PRODUCTS_API + "/**").hasAuthority("SELLER")
                .antMatchers(HttpMethod.PUT, PRODUCTS_API + "/**").hasAuthority("SELLER")
                .antMatchers(HttpMethod.DELETE, PRODUCTS_API + "/**").hasAuthority("SELLER")
                .antMatchers(HttpMethod.POST, ORDERS_API + "/**").hasAuthority("CUSTOMER")
                .antMatchers(HttpMethod.PUT, ORDERS_API + "/**").hasAuthority("CUSTOMER")
                .antMatchers(HttpMethod.DELETE, ORDERS_API + "/**").hasAuthority("CUSTOMER")
                .antMatchers(HttpMethod.GET, ORDERS_API + "/**").authenticated()
                .antMatchers(HttpMethod.POST, BASKET_API + "/**").hasAuthority("CUSTOMER")
                .antMatchers(HttpMethod.PUT, BASKET_API + "/**").hasAuthority("CUSTOMER")
                .antMatchers(HttpMethod.DELETE, BASKET_API + "/**").hasAuthority("CUSTOMER")
                .antMatchers(HttpMethod.GET, BASKET_API + "/**").authenticated()
                .antMatchers(HttpMethod.GET, SELLERS_LIST_API).hasAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE, SELLERS_LIST_API + "/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, CUSTOMER_LIST_API + "/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE, CUSTOMER_LIST_API + "/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, USER_PAGE_URL+ "/**").authenticated()
                .antMatchers(HttpMethod.PUT, USER_PAGE_URL+ "/**").authenticated();

    }
}
