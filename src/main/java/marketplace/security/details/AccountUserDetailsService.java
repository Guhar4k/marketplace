package marketplace.security.details;

import lombok.RequiredArgsConstructor;
import marketplace.repositories.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class AccountUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email)  throws UsernameNotFoundException {
        return new AccountUserDetails(
                userRepository.findUserByEmail(email)
                        .orElseThrow(() -> new UsernameNotFoundException("Пользователь не найден!")));
    }
}
