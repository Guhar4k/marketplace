package marketplace.repositories;

import marketplace.models.entities.Delivery;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DeliveriesRepository extends JpaRepository<Delivery, Long> {
    Optional<Delivery> findByName(String name);
}
