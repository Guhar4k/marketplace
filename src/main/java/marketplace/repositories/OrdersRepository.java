package marketplace.repositories;

import marketplace.models.entities.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrdersRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByOrderStatus_Name (String statusName, Pageable pageable);
    List<Order> findAllByCustomerId (Long customerId, Pageable pageable);
}
