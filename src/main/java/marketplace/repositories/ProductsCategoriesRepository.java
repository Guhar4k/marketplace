package marketplace.repositories;

import marketplace.models.entities.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductsCategoriesRepository extends JpaRepository<ProductCategory, Long> {
}
