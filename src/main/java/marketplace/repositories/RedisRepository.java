package marketplace.repositories;

public interface RedisRepository {
    void save(String token);

    void saveTokenAndUserId(String token, Long userId);

    boolean exists(String token);

    Long getUserIdByToken(String token);
}
