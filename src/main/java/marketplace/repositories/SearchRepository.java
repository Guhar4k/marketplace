package marketplace.repositories;

import java.util.List;
import marketplace.repositories.impl.search.SearchCriteria;
import marketplace.models.entities.Product;

public interface SearchRepository {
    List<Product> findByFilters(SearchCriteria searchData);
}
