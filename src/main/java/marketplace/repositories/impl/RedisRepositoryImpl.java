package marketplace.repositories.impl;

import lombok.RequiredArgsConstructor;
import marketplace.repositories.RedisRepository;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.concurrent.TimeUnit;

@Repository
@RequiredArgsConstructor
public class RedisRepositoryImpl implements RedisRepository {
    public static final int EXPIRE_TIMEOUT_DAYS = 1;

    private final RedisTemplate<String, String> redisTemplate;

    @Override
    public void save(String token) {
        redisTemplate.opsForValue().set(token, "");
    }

    @Override
    public void saveTokenAndUserId(String token, Long userId) {
        redisTemplate.opsForValue().set(token, userId.toString());
        redisTemplate.expire(token, EXPIRE_TIMEOUT_DAYS, TimeUnit.DAYS);
    }

    @Override
    public boolean exists(String token) {
        Boolean hasToken = redisTemplate.hasKey(token);
        return hasToken != null && hasToken;
    }

    @Override
    public Long getUserIdByToken(String token) {
        String value = redisTemplate.opsForValue().get(token);
        redisTemplate.delete(token);
        return Long.valueOf(value);
    }
}
