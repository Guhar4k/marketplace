package marketplace.repositories.impl.search;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.criteria.*;

import lombok.Data;
import lombok.NoArgsConstructor;
import marketplace.models.dto.Sorting;
import marketplace.models.entities.Product;
import marketplace.models.entities.ProductCategory;
import marketplace.models.entities.ProductCategory_;
import marketplace.models.entities.Product_;

@Data
@NoArgsConstructor
public class SearchCriteria {
    private String searchText;
    private Long categoryId;
    private Long sellerId;
    private Double minPrice;
    private Double maxPrice;
    private Sorting sorting;
    private int firstResult;
    private int pageSize;

    protected Predicate[] resolvePredicates(CriteriaBuilder criteriaBuilder, Root<Product> product) {
        SetJoin<Product, ProductCategory> productCategories = product.join(Product_.productCategories);
        List<Predicate> filters = new LinkedList<>();

        if (searchText != null) {
            filters.add(
                criteriaBuilder.or(
                    criteriaBuilder.like(
                        criteriaBuilder.lower(product.get(Product_.name)),
                        searchText
                    ),
                    criteriaBuilder.like(
                        criteriaBuilder.lower(product.get(Product_.description)),
                        searchText
                    )
                )
            );
        }

        if (categoryId != null) {
            filters.add(criteriaBuilder.equal(productCategories.get(ProductCategory_.category), categoryId));
        }

        if (sellerId != null) {
            filters.add(criteriaBuilder.equal(product.get(Product_.seller), sellerId));
        }

        filters.add(criteriaBuilder.gt(product.get(Product_.price), minPrice));

        if (maxPrice != null) {
            filters.add(criteriaBuilder.lt(product.get(Product_.price), maxPrice));
        }

        filters.add(criteriaBuilder.notEqual(product.get(Product_.isDeleted), true));
        filters.add(criteriaBuilder.notEqual(product.get(Product_.isVisible), false));

        Predicate[] predicates = new Predicate[filters.size()];
        predicates = filters.toArray(predicates);
        return predicates;
    }

    protected Order resolveSearchCriteria(CriteriaBuilder criteriaBuilder, Root<Product> product) {
        Order orderCriteria;
        if (sorting.equals(Sorting.NAME)) {
            orderCriteria = criteriaBuilder.asc(product.get(Product_.name));
        } else if (sorting.equals(Sorting.PRICE_DESC)) {
            orderCriteria = criteriaBuilder.desc(product.get(Product_.price));
        } else {
            orderCriteria = criteriaBuilder.asc(product.get(Product_.price));
        }
        return orderCriteria;
    }
}
