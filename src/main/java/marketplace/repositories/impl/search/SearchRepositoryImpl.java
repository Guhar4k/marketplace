package marketplace.repositories.impl.search;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import marketplace.models.entities.Product;
import marketplace.repositories.SearchRepository;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class SearchRepositoryImpl implements SearchRepository {

    private final EntityManager entityManager;

    @Override
    public List<Product> findByFilters(SearchCriteria searchCriteria) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
        Root<Product> product = criteriaQuery.from(Product.class);

        criteriaQuery.select(product)
            .where(searchCriteria.resolvePredicates(criteriaBuilder, product))
            .orderBy(searchCriteria.resolveSearchCriteria(criteriaBuilder, product));

        TypedQuery<Product> typedQuery = entityManager.createQuery(criteriaQuery);

        typedQuery.setFirstResult(searchCriteria.getFirstResult());
        typedQuery.setMaxResults(searchCriteria.getPageSize());

        return typedQuery.getResultList();
    }


}
