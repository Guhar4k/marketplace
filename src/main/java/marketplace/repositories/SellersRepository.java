package marketplace.repositories;


import marketplace.models.entities.User;
import marketplace.models.entities.Seller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellersRepository extends JpaRepository<Seller, Long> {
    Seller findSellerByUser(User user);
}
