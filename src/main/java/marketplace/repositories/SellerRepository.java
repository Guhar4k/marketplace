package marketplace.repositories;

import marketplace.models.entities.Seller;
import marketplace.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellerRepository extends JpaRepository<Seller, Long> {
    Seller findSellerByUser(User user);
}
