package marketplace.repositories;

import marketplace.models.entities.Product;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductsRepository extends JpaRepository<Product, Long> {

    List<Product> findAllByIsDeletedIsNotAndIsVisibleIs(Boolean isDeleted, Boolean isVisible, Pageable pageable);

    @Query("FROM Product p WHERE p.seller.id = ?1")
    List<Product> getProductsBySellerId(Long sellerId, Pageable pageable);

    @Query("FROM Product p " +
            "JOIN p.productCategories pc WHERE pc.category.id = ?1")
    List<Product> getProductsByCategoryId(Long categoryId, Pageable pageable);
}
