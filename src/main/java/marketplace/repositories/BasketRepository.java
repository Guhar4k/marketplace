package marketplace.repositories;

import marketplace.models.entities.Basket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BasketRepository extends CrudRepository<Basket, Long> {
    Iterable<Basket> findBasketDtoByCustomerId(Long customerId);
}
