package marketplace.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import marketplace.models.entities.User;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class AllUserDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private List<String> role;

    public static AllUserDto from(User user) {
        return AllUserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .role(user.getUserRoles()
                        .stream().map(userRole -> userRole.getRole().getName().toString())
                        .collect(Collectors.toList()))
                .build();
    }

    public static List<AllUserDto> from(List<User> users) {
        return users.stream().map(AllUserDto::from).collect(Collectors.toList());
    }

}
