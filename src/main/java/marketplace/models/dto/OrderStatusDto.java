package marketplace.models.dto;

import lombok.*;
import marketplace.models.entities.OrderStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderStatusDto {
    private Long id;
    private String name;

    public static OrderStatusDto from(OrderStatus orderStatus){
        return OrderStatusDto.builder()
                .id(orderStatus.getId())
                .name(orderStatus.getName())
                .build();
    }
}
