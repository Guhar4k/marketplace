package marketplace.models.dto;

public enum Sorting {
    NAME,
    PRICE,
    PRICE_DESC;
}
