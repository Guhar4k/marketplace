package marketplace.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import marketplace.models.entities.Product;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
    private Long id;
    private String name;
    private String description;
    private Double price;
    private Integer quantity;
    private Long sellerId;
    private String sellerName;
    private Boolean isDeleted;
    private Boolean isVisible;
    private String photoFile;

    public static ProductDto from(Product product) {
        if (product.getSeller() == null) {
            return ProductDto.builder()
                    .id(product.getId())
                    .name(product.getName())
                    .description(product.getDescription())
                    .price(product.getPrice())
                    .quantity(product.getQuantity())
                    .isDeleted(product.getIsDeleted())
                    .isVisible(product.getIsVisible())
                    .photoFile(product.getPhotoFile())
                    .build();
        } else {
            return ProductDto.builder()
                    .id(product.getId())
                    .name(product.getName())
                    .description(product.getDescription())
                    .price(product.getPrice())
                    .quantity(product.getQuantity())
                    .sellerId(product.getSeller().getId())
                    .sellerName(product.getSeller().getName())
                    .isDeleted(product.getIsDeleted())
                    .isVisible(product.getIsVisible())
                    .photoFile(product.getPhotoFile())
                    .build();
        }
    }

    public static List<ProductDto> from(List<Product> products) {
        return products.stream().map(ProductDto::from).collect(Collectors.toList());
    }
}
