package marketplace.models.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class SearchDto {
    private String text;
    private Long category;
    private Long seller;
    @Digits(fraction = 2, integer = 12)
    private String minPrice;
    @Digits(fraction = 2, integer = 12)
    private String maxPrice;
    private Sorting sorting = Sorting.PRICE;
    @NotNull(message = "Page cannot be null")
    @Min(1)
    private int page;
    @NotNull(message = "Page size cannot be null")
    @Min(1)
    private int size;
}


