package marketplace.models.dto;

import lombok.*;
import marketplace.models.entities.Order;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDto {
    private Long id;
    private Date orderDate;
    private String orderNum;
    private Double orderSum;
    private Long customerId;

    public static OrderDto from(Order order){
        return OrderDto.builder()
                .id(order.getId())
                .orderDate(order.getOrderDate())
                .orderNum(order.getOrderNum())
                .orderSum(order.getOrderSum())
                .customerId(order.getCustomer().getId())
                .build();
    }

    public static List<OrderDto> from(List<Order> orders){
        return orders.stream().map(OrderDto::from).collect(Collectors.toList());
    }
}