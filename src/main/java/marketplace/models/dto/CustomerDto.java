package marketplace.models.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerDto {
    private Long id;
    private Integer bonus;
    private Double discount;
    private Boolean isDeleted;
}
