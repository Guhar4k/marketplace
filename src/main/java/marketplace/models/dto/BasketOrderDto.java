package marketplace.models.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasketOrderDto {
    private Long basketId;
    private Long customerId;
    private Long productId;
    private Integer count;
    private Double sum;
}
