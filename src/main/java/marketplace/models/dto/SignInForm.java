package marketplace.models.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SignInForm {
    private String email;
    private String password;
}
