package marketplace.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import marketplace.models.entities.Basket;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasketDto {
    private Long basketId;
    private Long customerId;
    private Long productId;
    private Integer count;

    public static BasketDto from(Basket basket) {
        return BasketDto.builder()
                .basketId(basket.getBasketId())
                .customerId(basket.getCustomerId())
                .productId(basket.getProductId())
                .count(basket.getCount())
                .build();
    }

    public static List<BasketDto> from(List<Basket> baskets) {
        return baskets.stream().map(BasketDto::from).collect(Collectors.toList());
    }
}
