package marketplace.models.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SellerDto {
    private Long id;
    private String name;
    private String details;
    private Boolean isDeleted;
}
