package marketplace.models.dto;

import lombok.*;
import marketplace.models.entities.ProductOrder;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductOrderDto {
    private Long id;
    private Double price;
    private Integer quantity;

    private Long orderID;

    public static ProductOrderDto from(ProductOrder productOrder){

        return ProductOrderDto.builder()
                .id(productOrder.getId())
                .price(productOrder.getPrice())
                .quantity(productOrder.getQuantity())
                .build();
    }

    public static List<ProductOrderDto> from(List<ProductOrder> productOrders){
        return productOrders.stream().map(ProductOrderDto::from).collect(Collectors.toList());
    }
}
