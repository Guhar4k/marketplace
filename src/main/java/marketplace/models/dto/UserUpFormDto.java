package marketplace.models.dto;

/**
 * author Feygelman Gulnara
 * date 26.11.2021
 * project marketplace
 */
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import marketplace.models.entities.User;
import marketplace.validation.NotSameFields;
import marketplace.validation.NotSameFieldsUp;
import marketplace.validation.ValidPassword;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@NotSameFieldsUp
public class UserUpFormDto {
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotBlank
    @ValidPassword
    private String password;

    private String phone;

    private String address;
    
    public static UserUpFormDto from(User user) {
        return UserUpFormDto.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .password(user.getPassword())
                .phone(user.getPhone())
                .address(user.getAddress())
                .build();
    }

    public static List<UserUpFormDto> from(List<User> users) {
        return users.stream().map(UserUpFormDto::from).collect(Collectors.toList());
    }

}

