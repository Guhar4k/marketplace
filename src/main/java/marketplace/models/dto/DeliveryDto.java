package marketplace.models.dto;

import lombok.*;
import marketplace.models.entities.Delivery;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DeliveryDto {
    private Long id;
    private String name;

    public static DeliveryDto from (Delivery delivery){
        return DeliveryDto.builder()
                .id(delivery.getId())
                .name(delivery.getName())
                .build();
    }

    public static List<DeliveryDto> from(List<Delivery> deliveries){
        return deliveries.stream().map(DeliveryDto::from).collect(Collectors.toList());
    }
}
