package marketplace.models.dto;

import lombok.Data;

@Data
public class TokenRefreshDto {
    private String refreshToken;
}
