package marketplace.models.entities;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@RedisHash("Basket")
public class Basket {

    @Id
    private Long basketId;
    @Indexed
    private Long customerId;
    private Long productId;
    private Integer count;
}
