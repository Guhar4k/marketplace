package marketplace.models.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Role { //Справочник ролей пользователя
    public enum RoleName {
        ADMIN, CUSTOMER, SELLER
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private Long id;

    @Enumerated(value = EnumType.STRING)
    private RoleName name;

    @OneToMany(mappedBy = "role")
    @ToString.Exclude
    private List<UserRole> userRoles;
}
