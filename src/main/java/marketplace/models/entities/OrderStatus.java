package marketplace.models.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "order_status")
public class OrderStatus { //Справочник статусов заказов
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_status_id")
    private Long id;

    private String name;

    @OneToMany(mappedBy = "orderStatus")
    @ToString.Exclude
    private List<Order> orders;
}
