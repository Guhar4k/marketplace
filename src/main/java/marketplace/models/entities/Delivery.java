package marketplace.models.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Delivery {//Справочник типов доставки (например: Самовывоз, Почта России, PickPoint...)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "delivery_id")
    private Long id;

    private String name;

    @OneToMany(mappedBy = "delivery")
    @ToString.Exclude
    private List<Order> orders;
}
