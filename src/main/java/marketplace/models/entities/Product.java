package marketplace.models.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Long id;

    private String name;

    private String description;

    private Double price;

    private Integer quantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "seller_id")
    private Seller seller;

    @Column(name = "photo_file")
    private String photoFile;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Column(name = "is_visible")
    private Boolean isVisible;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
    private Set<ProductCategory> productCategories;
}
