package marketplace.repositories;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import marketplace.models.entities.Category;
import marketplace.models.entities.Product;
import marketplace.models.entities.ProductCategory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureEmbeddedDatabase(type = AutoConfigureEmbeddedDatabase.DatabaseType.POSTGRES,
        beanName = "dataSource", provider = AutoConfigureEmbeddedDatabase.DatabaseProvider.ZONKY,
        refresh = AutoConfigureEmbeddedDatabase.RefreshMode.BEFORE_EACH_TEST_METHOD)
public class ProductsRepositoryTest {
    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private CategoriesRepository categoriesRepository;

    @Autowired
    private ProductsCategoriesRepository productsCategoriesRepository;

    private void saveProducts() {
        Category category = Category.builder().id(1L).name("Бытовая техника").build();
        categoriesRepository.save(category);

        List<Product> products = Arrays.asList(
                Product.builder().id(1L).name("Чайник").isDeleted(false).isVisible(true).build(),
                Product.builder().id(2L).name("Телевизор").isDeleted(false).isVisible(true).build());

        productsRepository.saveAll(products);

        List<ProductCategory> productCategories = Arrays.asList(
                ProductCategory.builder().product(products.get(0)).category(category).build(),
                ProductCategory.builder().product(products.get(1)).category(category).build());

        productsCategoriesRepository.saveAll(productCategories);
    }

    @Test
    public void return_correct_counts_of_products() {
        saveProducts();
        assertThat(productsRepository.count()).isEqualTo(2L);
    }

    @Test
    public void return_correct_list_of_products() {
        saveProducts();
        List<Product> products = productsRepository.findAllByIsDeletedIsNotAndIsVisibleIs(true, true, Pageable.ofSize(5));

        assertThat(products)
                .extracting((Function<Product, String>) product -> product.getName() + " - " + product.getProductCategories().stream()
                        .map(productCategory -> productCategory.getCategory().getName())
                        .collect(Collectors.joining(",")))
                .contains("Чайник - Бытовая техника", "Телевизор - Бытовая техника");

    }
}
