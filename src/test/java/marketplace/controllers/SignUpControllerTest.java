package marketplace.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import marketplace.models.dto.SignUpForm;
import marketplace.models.dto.UserResponse;
import marketplace.models.entities.Category;
import marketplace.models.entities.Product;
import marketplace.models.entities.ProductCategory;
import marketplace.repositories.CategoriesRepository;
import marketplace.repositories.ProductsCategoriesRepository;
import marketplace.repositories.ProductsRepository;
import marketplace.services.ProductsService;
import marketplace.services.SignUpService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static marketplace.models.entities.Role.RoleName.CUSTOMER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.hamcrest.Matchers.is;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayName("SignUpController is working when")
public class SignUpControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SignUpService signUpService;

    @BeforeEach
    public void setUp() {
        when(signUpService.signUp(SignUpForm.builder()
                        .firstName("Ivan")
                        .lastName("Ivanov")
                        .email("ivan.ivanov@gmail.com")
                        .password("Qwerty007")
                        .build()))
                  .thenReturn(UserResponse.builder()
                        .firstName("Ivan")
                        .lastName("Ivanov")
                        .roleName(CUSTOMER)
                        .build());
        when(signUpService.signUp(SignUpForm.builder()
                .firstName("Tatyana")
                .lastName("Petrova")
                .email("tatyana.petrova@gmail.com")
                .password("Asdfgh111")
                .build())).thenThrow(IllegalArgumentException.class);
    }

    @Test
    @DisplayName("signUp() is working when")
    public void return_userResponse() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(post("/api/signUp")
                    .content(objectMapper.writeValueAsString(SignUpForm.builder()
                            .firstName("Ivan")
                            .lastName("Ivanov")
                            .email("ivan.ivanov@gmail.com")
                            .password("Qwerty007")
                            .build()))
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("Ivan")))
                .andExpect(jsonPath("$.lastName", is("Ivanov")))
                .andExpect(jsonPath("$.roleName", is("CUSTOMER")));
    }

    @Test
    @DisplayName("signUp() is not working when")
    public void return_signup_throws() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(post("/api/signUp")
                        .content(objectMapper.writeValueAsString(SignUpForm.builder()
                                .firstName("Tatyana")
                                .lastName("Petrova")
                                .email("tatyana.petrova@gmail.com")
                                .password("Asdfgh111")
                                .build()))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }


}
