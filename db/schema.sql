insert into seller(details, is_deleted, name, user_id)
values ('Продавец книг', false, 'Издательство ABC', 6);

insert into seller(details, is_deleted, name, user_id)
values ('Продавец товаров для дома', false, 'ООО Новогоднее настроени', 7);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('Большинство людей не любят математику, потому что считают ее скучной и сложной. Математика, на самом деле, наука интересная и захватывающая.',
        false,
        true,
        'Большой роман о математике | Лонэ Микаэль',
        null,
        560.00,
        5,
        1);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('"Автостопом по Галактике", стартовав в качестве радиопостановки на Би-би-си, имел грандиозный успех.',
        false,
        true,
        'Автостопом по галактике | Адамс Дуглас Ноэль',
        null,
        550.00,
        5,
        1);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('Это книга, меняющая мировоззрение, она формирует целостное видение мира и дает ответы на вопросы о смысле человеческой жизни и общественном значении предпринимательства.',
        false,
        true,
        'Атлант расправил плечи. В 3 книгах.',
        null,
        820.00,
        5,
        1);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('Космос никогда не казался таким близким, а Земля такой удивительной.',
        false,
        true,
        'Руководство астронавта по жизни на Земле. Чему научили меня 4000 часов на орбите',
        null,
        450.00,
        5,
        1);


insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('Каждая беседа пожилого человека с молодыми оборачивается поучением.',
        false,
        true,
        'Письма о добром и прекрасном | Лихачёв Дмитрий',
        null,
        570.00,
        5,
        1);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('[Новый год-2022] Красивая пушистая зеленая елка.',
        false,
        true,
        'Елка новогодняя 2м',
        null,
        1550.00,
        5,
        2);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('[Новый год-2022] Мерцающая гирлянда для новогодней атмосферы.',
        false,
        true,
        'Гирлянда светодиодная 3м',
        null,
        550.00,
        5,
        2);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('[Новый год-2022] Яркие стеклянные игрушки из детства.',
        false,
        true,
        'Набор новородних игрушек 18шт',
        null,
        1000.00,
        5,
        2);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('[Новый год-2022] Для бутербродов.',
        false,
        true,
        'Ведро красной икры (батон в подарок)',
        null,
        2000.00,
        5,
        2);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('[Новый год-2022] Для настоящего праздника.',
        false,
        true,
        'Бенгальские свечи 12шт',
        null,
        200.00,
        5,
        2);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('[Новый год-2022] Чтобы за второй и третьей не бегать.',
        false,
        true,
        'Шампанское упаковка 3шт',
        null,
        1500.00,
        5,
        2);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('[Новый год-2022] Новогодний фильм.',
        false,
        true,
        'DVD Диск Ирония Судьбы',
        null,
        300.00,
        5,
        1);

insert into product (description, is_deleted, is_visible, name, photo_file, price, quantity, seller_id)
values ('[Новый год-2022] Чтобы дзынь дзынь.',
        false,
        true,
        'Бокалы для шампанского набор 6шт',
        null,
        700.00,
        5,
        2);

insert into category(name)
values ('Книги');
insert into category(name)
values ('Новогодние товары');

insert into product_category(category_id, product_id)
values (4, 6);

insert into product_category(category_id, product_id)
values (4, 7);

insert into product_category(category_id, product_id)
values (4, 8);

insert into product_category(category_id, product_id)
values (4, 9);

insert into product_category(category_id, product_id)
values (4, 10);

insert into product_category(category_id, product_id)
values (5, 11);

insert into product_category(category_id, product_id)
values (5, 12);

insert into product_category(category_id, product_id)
values (5, 13);

insert into product_category(category_id, product_id)
values (5, 14);

insert into product_category(category_id, product_id)
values (5, 15);

insert into product_category(category_id, product_id)
values (5, 16);

insert into product_category(category_id, product_id)
values (5, 17);

insert into product_category(category_id, product_id)
values (5, 18);

insert into customer(bonus, discount, is_deleted, user_id)
values (20, 2, null, 4);

insert into customer(bonus, discount, is_deleted, user_id)
values (100, 8, null, 5);

insert into user (address, email, firstName, lastName, password, phone, state)
values ('Kazan',
        'admin@mail.ru',
        'Vasia',
        'Vasilev',
        '$2a$10$c8wRrWlietASn8GoEAE3fuZK7kyxgNjZhNjqGX4jYXiB4bnMtiIlC',
        '89171112299',
        'CONFIRMED');

insert into user (address, email, firstName, lastName, password, phone, state)
values ('Kazan',
        'johnny@gmail.com',
        'Johnny',
        'Cash',
        '$2a$10$Uyr9UZ9Px8W7etcTQoMQjOp7.NdvrTY35yP8QRSnMXLLzZPojTQu6',
        '89170001122',
        'CONFIRMED');

insert into user (address, email, firstName, lastName, password, phone, state)
values ('Sankt-Peterburg',
        'spb@yandex.ru',
        'Aleksander',
        'Pakhmutov',
        '$2a$10$Uyr9UZ9Px8W7etcTQoMQjOp7.NdvrTY35yP8QRSnMXLLzZPojTQu6',
        '89371112233',
        'DELETED');

insert into user (address, email, firstName, lastName, password, phone, state)
values ('Abakan',
        'customer1@gmail.com',
        'Custo1',
        'Custov1',
        '$2a$10$QUCUid9pMP7hAPCRc86Li.p.5u9z4/PXQ2JcYqI22drRgcZd7sPFy',
        '89172212299',
        'CONFIRMED');

insert into user (address, email, firstName, lastName, password, phone, state)
values ('Innopolis',
        'customer2@mail.com',
        'Custo2',
        'Custov2',
        '$2a$10$5nJRYpLK2K7suWpm.Ig4DOMZgtX//KtDvKT/38KFqdnRSOUe0WQqu',
        '89173312299',
        'CONFIRMED');

insert into user (address, email, firstName, lastName, password, phone, state)
values ('Moscow',
        'seller1@list.ru',
        'Sell1',
        'Seller1',
        '$2a$10$RCrDcKhcSnlu02GSMXvmNO9aJZ96GJ9Wa.UhDGAyrcQ8OdCQG9yJ2',
        '89194412299',
        'CONFIRMED');


insert into user (address, email, firstName, lastName, password, phone, state)
values ('Voronez',
        'seller2@list.ru',
        'Sell2',
        'Seller2',
        '$2a$10$zj03q.mf0TI6tLc3dePCmOhG4la3J3IEbndEf1fadxGtHiW/G2PPC',
        '89195512299',
        'CONFIRMED');

insert into user_role(role_id, user_id)
values (1, 1);

insert into user_role(role_id, user_id)
values (2, 2);

insert into user_role(role_id, user_id)
values (2, 3);

insert into user_role(role_id, user_id)
values (2, 4);

insert into user_role(role_id, user_id)
values (2, 5);

insert into user_role(role_id, user_id)
values (3, 6);

insert into user_role(role_id, user_id)
values (3, 7);
